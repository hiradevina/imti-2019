from django.db import models
from datetime import datetime,date
from django.utils import timezone
# Create your models here.

class NewsEvent(models.Model):
	judul = models.CharField(max_length = 100 , primary_key = True)
	isi = models.TextField(default ='Artikel IMTI')
	foto = models.CharField(max_length = 350, default='#')

class NewsPO(models.Model):
	judul = models.CharField(max_length = 100, primary_key = True)
	isi = models.TextField(default ='Pengumuman PO')
	foto = models.CharField(max_length = 350, default='#')
