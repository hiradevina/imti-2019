from django.shortcuts import render
from .models import *
from django.shortcuts import get_object_or_404

# Create your views here.

def NewsView(request, judul):
	news = get_object_or_404(NewsEvent, pk=judul)
	return render(request, 'news/EventBaru.html', { 
		'judul':news.judul, 'isi':news.isi, 'foto':news.foto
		})

def POView(request, judul):
	news = get_object_or_404(NewsPO, pk=judul)
	return render(request, 'news/PObaru.html', { 
		'judul':news.judul, 'isi':news.isi, 'foto':news.foto
		})

def CardEvent(request):
	return render(request,'news/EventCard.html')

def POEvent(request):
	return render(request, 'news/POCard.html')

