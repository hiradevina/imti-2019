from django.shortcuts import render
from .models import Investiga as InvestigaData
# Create your views here.
def investiga(request):
    respond = {}
    datas = InvestigaData.objects.all()
    respond['datas'] = datas
    return render(request, 'investiga/investiga.html', respond)